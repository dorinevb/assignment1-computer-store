// Elements of the bank section
const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstanding-loan");
const getLoanButton = document.getElementById("get-loan");

// Elements of the work section
const currentPayElement = document.getElementById("current-pay");
const transferBankButton = document.getElementById("transfer-bank");
const increasePayButton = document.getElementById("increase-pay");
const repayLoanButton = document.getElementById("repay-loan");

// Elements of the laptops section
const laptopInfoElement = document.getElementById("laptop-info");
const laptopSelectElement = document.getElementById("laptop-select");
const featuresHeaderElement = document.getElementById("features-header");
const laptopFeaturesElement = document.getElementById("laptop-features");
const laptopHeaderElement = document.getElementById("laptop-header");
const laptopImageElement = document.getElementById("laptop-image");
const laptopNameElement = document.getElementById("laptop-name");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("laptop-price");
const buyNowButton = document.getElementById("buy-now");

// Elements of the Wish List section
const wishlistElement = document.getElementById("wishlist");
const addWishlistButton = document.getElementById("add-wishlist");
const wishlistItemsElement = document.getElementById("wishlist-items");
const clearWishlistButton = document.getElementById("clear-wishlist");

// Constants
const PERCENTAGE_TO_LOAN = 0.1; //This percentage of salary goes to paying off loan (0.1 => 10%)
const FUNDS_FACTOR = 2; // Loan can be max this factor times bank balance

// Global variables
let balance = 0;
let outstandingLoan = 0;
let currentPay = 0;
let laptops = [];
let laptopInfo = "";
let featuresList = [];
let wishlistItems = {};

// Convert number to Euro format
const numToEuro = (aNumber) => {
  return new Intl.NumberFormat('nl-NL',
    { style: 'currency', currency: 'EUR' }).format(aNumber);
}

// Default settings
balanceElement.innerText = `Balance: ${numToEuro(balance)}`;
outstandingLoanElement.style.display = "none";
repayLoanButton.style.display = "none";
currentPayElement.innerText = `Pay: ${numToEuro(currentPay)}`;
laptopInfoElement.style.display = "none";
wishlistElement.style.display = "none";

// Functionality of the bank section

// Event listener for the Get Loan Button
getLoanButton.addEventListener("click", handleLoanRequest);

// Handles loan request when user clicks Get Loan Button
function handleLoanRequest() {
  if (outstandingLoan > 0) { // Check if user already has a loan
    alert("You cannot have more than one loan.");
  } else {
    let requestedAmount = getAmount(); // Get input from user
    if (requestedAmount) { // Check if there is valid input
      let enoughFunds = checkBalance(requestedAmount); // Check if user has sufficient funds
      if (enoughFunds) { 
        outstandingLoan = requestedAmount;
        outstandingLoanElement.innerText = `Outstanding loan: ${numToEuro(outstandingLoan)}`
        outstandingLoanElement.style.display = "block";
        repayLoanButton.style.display = "block";
      } 
    }
  }
}

// Prompts user for loan amount and checks if input is valid
function getAmount() {
  try {
    // Get input, remove spaces and convert to float
    const inputAmount = parseFloat(prompt("Enter loan amount:", "").trim()).toFixed(2); // empty default value so IE doesn´t show 'undefined'
    if (isNaN(inputAmount)) { // Check if input is valid number
      alert("Invalid input. Please enter a number.");
      return false;
    }
    return inputAmount;
  }
  catch (err) { // Catch any unexpected errors
    console.log(err);
    alert("Invalid input. Please enter a number.");
    return false;
  }
}

// Checks if user has sufficient funds for loan
function checkBalance(requestedLoanAmount) {
  if (balance * FUNDS_FACTOR < requestedLoanAmount) {
    alert("Insufficient funds.");
    return false;
  } else {
    return true;
  }
}

// Functionality of the work section

// Event listener for the Increase Pay Button (Work Button)
increasePayButton.addEventListener("click", () => {
  currentPay += 100;
  currentPayElement.innerText = `Pay: ${numToEuro(currentPay)}`;
});

// Event listener for the Transfer Bank Button
transferBankButton.addEventListener("click", handleBankTransfer);

// Handles bank transfer when user clicks Bank Button
function handleBankTransfer() {
  // If there is a loan, percentage goes to paying of loan
  if (outstandingLoan >= currentPay * PERCENTAGE_TO_LOAN) {
    outstandingLoan -= currentPay * PERCENTAGE_TO_LOAN;
    outstandingLoanElement.innerText = `Outstanding loan: ${numToEuro(outstandingLoan)}`
    balance += currentPay * (1 - PERCENTAGE_TO_LOAN);
    balanceElement.innerText = `Balance: ${numToEuro(balance)}`
  // If outstanding loan is smaller than the percentage, more goes to balance
  } else if (0 < outstandingLoan < currentPay * PERCENTAGE_TO_LOAN) {
    balance += (currentPay - outstandingLoan);
    balanceElement.innerText = `Balance: ${numToEuro(balance)}`
    outstandingLoan = 0;
    outstandingLoanElement.innerText = `Outstanding loan: ${numToEuro(outstandingLoan)}`
    // If there is no loan, everything goes to balance
  } else if(outstandingLoan === 0) {
    balance += currentPay;
    console.log(balance);
    balanceElement.innerText = `Balance: ${numToEuro(balance)}`
  }
  // Reset current pay to 0
  currentPay = 0;
  currentPayElement.innerText = `Pay: ${numToEuro(currentPay)}`;
  // If loan has been reduced to 0, stop displaying loan
  if (outstandingLoan === 0) {
    outstandingLoanElement.style.display = "none";
    repayLoanButton.style.display = "none";
  }
}

// Event listener for the Repay Loan Button
repayLoanButton.addEventListener("click", handleRepayLoan);

// Transfers full pay to outstanding loan
function handleRepayLoan() {
  if (outstandingLoan >= currentPay) {
    outstandingLoan -= currentPay;
    // Any remaining money after loan is fully paid off goes to balance
  } else if (outstandingLoan < currentPay) {
    balance += (currentPay - outstandingLoan);
    balanceElement.innerText = `Balance: ${numToEuro(balance)}`
    outstandingLoan = 0;
  }
  outstandingLoanElement.innerText = `Outstanding loan: ${numToEuro(outstandingLoan)}`
  currentPay = 0;
  currentPayElement.innerText = `Pay: ${numToEuro(currentPay)}`;
  if (outstandingLoan === 0) {
    outstandingLoanElement.style.display = "none";
    repayLoanButton.style.display = "none";
  }
}

// Functionality of the Laptops section

// Fetch laptop data from API, store in array and add to select element
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then(response => response.json())
  .then(data => laptops = data)
  .then(laptops => addLaptopsToSelect(laptops))
  .catch(error => console.error("Error: ", error.message));

// Perform the convert function on each laptop item
const addLaptopsToSelect = (laptops) => {
  laptops.forEach(laptop => convertLaptopToOption(laptop));
}

// Convert laptop item to option and add to select element
const convertLaptopToOption = (laptop) => {
  const optionElement = document.createElement("option");
  optionElement.text = laptop.title;
  laptopSelectElement.appendChild(optionElement);
}

// Event listener for select menu
laptopSelectElement.addEventListener("change", (e) => {
  const currentLaptop = e.target.value;
  laptopInfo = laptops.find(laptop => laptop.title === currentLaptop);
  displayFeatures(laptopInfo.specs);
  displayLaptop(laptopInfo);
});

// Remove children from ul element
function emptyList(elementToEmpty) {
  while (elementToEmpty.firstChild) {
    elementToEmpty.removeChild(elementToEmpty.firstChild);
  }
}

// Perform the convert function on each laptop feature
const displayFeatures = (listFromJson) => {
  emptyList(laptopFeaturesElement);
  featuresHeaderElement.innerText = "Features:";
  listFromJson.forEach(feature => convertFeatureToLi(feature));
}

// Convert feature to li element and add to list
const convertFeatureToLi = (feature) => {
  const liElement = document.createElement("li");
  const liText = document.createTextNode(feature);
  liElement.appendChild(liText);
  laptopFeaturesElement.appendChild(liElement);
}

// Event listener for error when loading image
laptopImageElement.addEventListener("error", () => {
  laptopImageElement.src = "https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-5529.jpg?t=st=1649600718~exp=1649601318~hmac=044bd1bfc3dd857100fbddda9aec5fee4144283a3969e77373874c78b503b906&w=740"
})

// Displays info about the selected laptop
function displayLaptop(info) {
  laptopInfoElement.style.display = "block";
  laptopHeaderElement.innerText = "Laptop Info";
  laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + info.image;
  laptopNameElement.innerText = info.title;
  laptopDescriptionElement.innerText = info.description;
  laptopPriceElement.innerText = `Price: ${numToEuro(info.price)}`;
}

// Event listener for the Buy Now Button
buyNowButton.addEventListener("click", handleBuyLaptop);

// Checks if user has enough funds and if so, buys laptop
function handleBuyLaptop() {
  laptopPrice = parseFloat(laptopInfo.price).toFixed(2);
  if (laptopPrice > balance) {
    alert("You have insufficient funds to buy this laptop.");
  } else {
    balance -= laptopPrice;
    balanceElement.innerText = `Balance: ${numToEuro(balance)}`
    alert(`You are now the proud owner of a ${laptopInfo.title}!`);
  }
}

// Functionality of the Wish List section

// Event listener for the Add to Wish List button
addWishlistButton.addEventListener("click", addToWishlist);

// Updates wish list if user adds item
function addToWishlist() {
  checkOnWishlist();
  emptyList(wishlistItemsElement);
  fillWishlist();
  wishlistElement.style.display = "block";
}

// Checks if item is already on wish list and changes count
function checkOnWishlist() {
  if (laptopInfo.title in wishlistItems) {
    wishlistItems[laptopInfo.title] += 1;
  } else {
    wishlistItems[laptopInfo.title] = 1;
  }
}

// Performs convert function on each wish list item
function fillWishlist() {
  for (item in wishlistItems) {
    convertItemToLi(item);
  }
}

// Converts item to li element and appends to wish list
function convertItemToLi(item) {
  const liElement = document.createElement("li");
  const liText = document.createTextNode(`${item} (${wishlistItems[item]})`);
  liElement.appendChild(liText);
  wishlistItemsElement.appendChild(liElement);
}

// Event listener for Clear Wish List button
clearWishlistButton.addEventListener("click", clearWishlist);

// Removes all items from wish list
function clearWishlist() {
  emptyList(wishlistItemsElement);
  wishlistItems = {};
}