### Computer Store Assignment
I coded this computer store template as an assignment for the JavaScript course    
at Noroff School of Technology and Digital Media. 

The app consists of a frontend and uses JavaScript, HTML and CSS.    

The app has the following sections:    
- a Bank section where the user can check his/ her bank balance and request a loan.
- a Work section where the user can add salary, make transfers and pay off the loan.   
- a Laptops section where the user can select different laptops and buy them.
- a Wishlist section where the user can add laptops to his/ her wishlist.

As an extra practice I deployed the app to Heroku:    
https://the-ultimate-computer-store.herokuapp.com/    

